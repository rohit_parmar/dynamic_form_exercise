import React from 'react';

import LeftSidebar from './components/LeftSidebar';
import { AppProvider } from "./context/AppContext";

function App() {
  return (
    <AppProvider>
      <LeftSidebar />
    </AppProvider>
  );
}

export default App;
