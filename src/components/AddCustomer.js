import React, { useContext, memo, useEffect, useState } from 'react';
import { DispatchContext, CustomersContext, FormContext, FormDispatchContext } from '../context/AppContext';
import { Paper, TextField, Button } from '@material-ui/core';
import { v4 as uuid } from 'uuid';
import { useHistory } from "react-router-dom";
import Builder from '../formbuilder/Builder';
import { resetState } from '../actions/formBuilderActions';
import { useSnackbar } from 'notistack';
import { Box, FormControl, InputLabel, Select, MenuItem } from '@material-ui/core';

function AddCustomer({ customerId }) {

  const [value, setValue] = useState('');
  const [matrixValue, setMatrixValue] = useState(12);
  const customerDispatch = useContext(DispatchContext);
  const customers = useContext(CustomersContext);
  const formState = useContext(FormContext);
  const formDispatch = useContext(FormDispatchContext);
  let history = useHistory();
  const { enqueueSnackbar } = useSnackbar();

  useEffect(() => {
    if (customerId) {
      // eslint-disable-next-line eqeqeq
      let currentCustomer = customers.find((element) => { return element.id == customerId });
      if (currentCustomer) {
        setValue(currentCustomer.customerName);
        setMatrixValue(currentCustomer.customerMatrix);
        let customerForm = JSON.parse(currentCustomer.customerForm);
        formDispatch({ type: 'EDIT_CUSTOMER', previewItems: customerForm });
      }
    } else {
      formDispatch(resetState());
      setMatrixValue(12);
    }
  }, [customerId, customers, formDispatch]);

  function checkCustomerName(customerName) {
    if (!customerName) {
      return true;
    }
    return customers.some(cutomer => cutomer.customerName === customerName);
  }

  function addCustomer(e) {
    e.preventDefault();
    if (customerId) {
      let customerForm = JSON.stringify(formState.previewItems);
      customerDispatch({ type: "EDIT_CUSTOMER", id: customerId, customerName: value, customerForm: customerForm, customerRecords: [], customerMatrix: matrixValue });
      history.push("/customer/" + customerId + '/add-record');
      enqueueSnackbar('Customer Updated Successfully', { variant: 'success' });
    } else {
      if (checkCustomerName(value)) {
        enqueueSnackbar('Dear customer your name is empty or already taken, Please another name.', { variant: 'error' });
      } else {
        let customerId = uuid();
        let customerForm = JSON.stringify(formState.previewItems);
        customerDispatch({ type: "ADD", id: customerId, customerName: value, customerForm: customerForm, customerRecords: [], customerMatrix: matrixValue });
        enqueueSnackbar('Customer Added Successfully', { variant: 'success' });
        history.push("/customer/" + customerId + '/add-record');
      }
    }
    formDispatch(resetState());
    setValue('');
    setMatrixValue(12);
  }

  return (
    <Paper style={{ margin: "1rem 0", padding: "1rem", height: '100%' }}>

      <div>
        <TextField
          value={value}
          onChange={e => setValue(e.target.value)}
          margin='normal'
          label='Enter Customer Name'
          fullWidth
        />
        <FormControl fullWidth mt={2}>
          <InputLabel >Form Matrix</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={matrixValue}
            onChange={e => setMatrixValue(e.target.value)}>
            <MenuItem value={12}>1 X 1 (1 Column)</MenuItem>
            <MenuItem value={6}>2 X 2 (2 Column)</MenuItem>
            <MenuItem value={4}>3 X 3 (3 Column)</MenuItem>
            <MenuItem value={3}>4 X 4 (4 Column)</MenuItem>
          </Select>
        </FormControl>
        <Box display="flex" justifyContent="center" alignItems="flex-end">
          <Builder editorVisible={formState.editorVisible} previewItems={formState.previewItems} />
        </Box>
        <Box display="flex" justifyContent="center" alignItems="flex-end">
          <Button variant="contained" type="submit" color="primary" onClick={e => addCustomer(e)} style={{ marginBottom: 10 }}>
            Submit
          </Button>
        </Box>
      </div>
    </Paper>
  );

}

export default memo(AddCustomer);