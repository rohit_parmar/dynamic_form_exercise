import React, { useContext } from 'react';
import { CustomersContext, DispatchContext } from '../context/AppContext';
import Generator from "../formgenerator/Generator";
import { Container, Button, Box } from '@material-ui/core';
import { useHistory } from "react-router-dom";
import { useSnackbar } from 'notistack';

function CusotmerForm({ customerId, recordId }) {
    const customers = useContext(CustomersContext);
    const customerDispatch = useContext(DispatchContext);
    let history = useHistory();
    const { enqueueSnackbar } = useSnackbar();
    function getCustomerRecord(customerId, recordId) {
        // eslint-disable-next-line eqeqeq
        let currentCustomer = customers.find((element) => { return element.id == customerId });
        if (currentCustomer && currentCustomer.customerRecords) {
            let records = currentCustomer.customerRecords.find((element) => { return element.recordId === recordId });
            return records;
        }
        return {};
    }
    function getCustomerFormData(customerId) {
        // eslint-disable-next-line eqeqeq
        let currentCustomer = customers.find((element) => { return element.id == customerId });
        if (currentCustomer && currentCustomer.customerForm) {
            return JSON.parse(currentCustomer.customerForm);
        }
        return [];
    }

    function getCustomerRecordsData(customerId) {
        let currentCustomer = customers.find((element) => { return element.id === customerId });
        if (currentCustomer && currentCustomer.customerRecords) {
            return currentCustomer.customerRecords
        }
        return [];
    }

    const onSubmitFunc = (items) => {
        var customerRecordsArray = getCustomerRecordsData(customerId);
        if (recordId) {
            customerRecordsArray = customerRecordsArray.map(customerRecord => {
                if (customerRecord.recordId === items.recordId) {
                    customerRecord = items;
                }
                return customerRecord;
            });
            enqueueSnackbar('Record Updated Successfully', { variant: 'success' });
        } else {
            customerRecordsArray.push(items);
            enqueueSnackbar('Record Added Successfully', { variant: 'success' });
        }
        customerDispatch({ type: "EDIT", id: customerId, customerRecords: customerRecordsArray });
        goToCustomerList();
    };

    function goToCustomerList() {
        history.push("/customer/" + customerId);
    }
    function getCustomerMatrix() {
        // eslint-disable-next-line eqeqeq
        let currentCustomer = customers.find((element) => { return element.id == customerId });
        if (currentCustomer && currentCustomer.customerMatrix) {
            return currentCustomer.customerMatrix;
        }
        return 12;
    }

    return (
        <div>
            <Container>
                <Generator matrix={getCustomerMatrix()} onSubmit={onSubmitFunc} formData={getCustomerFormData(customerId)} editData={getCustomerRecord(customerId, recordId)} />
                <Box display="flex" justifyContent="center" alignItems="flex-end">
                    <Button onClick={e => goToCustomerList()} variant="contained" type="submit" color="primary" style={{ margin: 25 }}>View Records</Button>
                </Box>
            </Container>
        </div>
    );

}

export default CusotmerForm;