import React, { useContext } from 'react';
import MaterialTable from 'material-table';
import { Container, Button } from '@material-ui/core';
import { CustomersContext, DispatchContext } from '../context/AppContext';
import { useHistory } from "react-router-dom";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Box from '@material-ui/core/Box';
import { useSnackbar } from 'notistack';

function CustomerList({ customerId }) {

    const customers = useContext(CustomersContext);
    const customerDispatch = useContext(DispatchContext);
    let history = useHistory();
    const { enqueueSnackbar } = useSnackbar();
    const [open, setOpen] = React.useState(false);
    const [selectedRecord, setSelectedRecord] = React.useState({});

    const handleClickOpen = (rowData) => {
        setSelectedRecord(rowData);
        setOpen(true);
    };

    const handleClose = () => {
        setSelectedRecord({});
        setOpen(false);

    };

    function getCustomerRecordsData(customerId) {
        let currentCustomer = customers.find((element) => { return element.id === customerId });
        if (currentCustomer && currentCustomer.customerRecords) {
            return currentCustomer.customerRecords
        }
        return [];
    }

    function deleteRecord() {
        var customerRecordsArray = getCustomerRecordsData(customerId);
        if (selectedRecord && selectedRecord.recordid) {
            customerRecordsArray = customerRecordsArray.filter((item) => item.recordId !== selectedRecord.recordid);
        }
        customerDispatch({ type: "EDIT", id: customerId, customerRecords: customerRecordsArray });
        handleClose();
        enqueueSnackbar('Record Deleted Successfully', { variant: 'success' });
    }

    function getCustomerName(customerId) {
        // eslint-disable-next-line eqeqeq
        let currentCustomer = customers.find((element) => { return element.id == customerId });
        if (currentCustomer && currentCustomer.customerName) {
            return currentCustomer.customerName;
        }
        return "";
    }

    function getTableColumnsList(customerId) {
        let currentCustomer = customers.find((element) => { return element.id === customerId });
        if (currentCustomer && currentCustomer.customerRecords) {
            var columnsData = [];
            if (currentCustomer.customerRecords.length) {
                for (let [key] of Object.entries(currentCustomer.customerRecords[0])) {
                    if (!key.includes('recordId')) {
                        let obj = { title: key, field: key.toLocaleLowerCase() };
                        columnsData.push(obj);
                    }
                };
            }
            return columnsData;
        }
        return [];
    }

    function getTableRowListData(customerId) {
        let currentCustomer = customers.find((element) => { return element.id === customerId });
        if (currentCustomer && currentCustomer.customerRecords) {
            var rowData = [];
            currentCustomer.customerRecords.forEach(element => {
                var rowDataObject = {};
                for (let [key, value] of Object.entries(element)) {
                    let k = key.toLocaleLowerCase();
                    rowDataObject[k] = value;
                };
                rowData.push(rowDataObject);
            });
            return rowData;
        }
        return [];
    }

    function goToAddCustomerRecord() {
        history.push("/customer/" + customerId + '/add-record');
    }

    function goToEditCutomerRecord(rowData) {
        let recordId = rowData.recordid;
        history.push("/customer/" + customerId + '/edit-record/' + recordId);
    }

    return (

        <Container>
            <MaterialTable
                title={`${getCustomerName(customerId)}`}
                columns={getTableColumnsList(customerId)}
                data={getTableRowListData(customerId)}
                actions={[
                    {
                        icon: 'edit',
                        tooltip: 'Edit User',
                        onClick: (event, rowData) => goToEditCutomerRecord(rowData)
                    },
                    rowData => ({
                        icon: 'delete',
                        tooltip: 'Delete User',
                        onClick: (event, rowData) => handleClickOpen(rowData),
                        disabled: rowData.birthYear < 2000
                    })
                ]}
                options={{
                    actionsColumnIndex: -1,
                    paging: false

                }}
            />

            <Box display="flex" justifyContent="center" alignItems="flex-end" m={3}>
                <Button onClick={e => goToAddCustomerRecord()} variant="contained" type="submit" color="primary">Add Record</Button>
            </Box>
            <div>
                <Dialog
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description">
                    <DialogTitle id="alert-dialog-title">{"Delete Record"}</DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                            Are you sure,You want to delete this record.
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleClose} color="primary">No</Button>
                        <Button onClick={(e) => deleteRecord(e)} color="primary" autoFocus>Yes</Button>
                    </DialogActions>
                </Dialog>
            </div>
        </Container>
    );
}
export default CustomerList;