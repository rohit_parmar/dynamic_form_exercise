import React, { useContext, memo, useEffect } from 'react';
import { DispatchContext, CustomersContext } from '../context/AppContext';
import {
  Container, Dialog, DialogActions,
  DialogContent, DialogContentText,
  DialogTitle, Box, Paper, List,
  ListItem, Button, IconButton,
  ListItemAvatar, Avatar, ListItemText,
  ListItemSecondaryAction
} from '@material-ui/core';
import { useSnackbar } from 'notistack';
import FolderIcon from "@material-ui/icons/Folder";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from '@material-ui/icons/Edit';
import { useHistory } from "react-router-dom";


function Home() {

  const customerDispatch = useContext(DispatchContext);
  const customers = useContext(CustomersContext);
  let history = useHistory();
  const [open, setOpen] = React.useState(false);
  const [selectedRecord, setSelectedRecord] = React.useState('');
  const { enqueueSnackbar } = useSnackbar();

  const handleClickOpen = (customerId) => {
    setSelectedRecord(customerId);
    setOpen(true);
  };

  const handleClose = () => {
    setSelectedRecord('');
    setOpen(false);
  };

  useEffect(() => {
    if (customers && customers.length === 0) {
      history.push("/add-customer");
    }
  }, [customers, history]);


  function addNewCustomer() {
    history.push("/add-customer");
  }
  function goToCustomer(customerId) {
    history.push("/customer/" + customerId + '/add-record');
  }
  function editCustomer(customerId) {
    history.push("/edit-customer/" + customerId);
  }
  function deleteCustomer() {
    customerDispatch({ type: "DELETE", id: selectedRecord });
    handleClose();
    enqueueSnackbar('Customer Deleted Successfully', { variant: 'success' });
  }

  return (
    <Container maxWidth="sm">
      <Paper style={{ margin: "1rem 0", padding: "1rem", height: '100%' }}>

        <div>

          <List>
            {customers.map((todo, i) => (
              <ListItem key={i} onClick={(e) => goToCustomer(todo.id, e)}>
                <ListItemAvatar>
                  <Avatar>
                    <FolderIcon />
                  </Avatar>
                </ListItemAvatar>
                <ListItemText
                  primary={todo.customerName}
                />
                <ListItemSecondaryAction>
                  <IconButton edge="end" aria-label="delete" onClick={(e) => editCustomer(todo.id, e)}>
                    <EditIcon />
                  </IconButton>
                  <IconButton edge="end" aria-label="delete" onClick={(e) => handleClickOpen(todo.id, e)}>
                    <DeleteIcon />
                  </IconButton>
                </ListItemSecondaryAction>
              </ListItem>
            ))}
          </List>


          <Box display="flex" justifyContent="center" alignItems="flex-end">
            <Button variant="contained" type="submit" color="primary" onClick={e => addNewCustomer(e)} style={{ marginBottom: 10 }}>
              Add Customer
          </Button>
          </Box>
        </div>
      </Paper>
      <div>
        <Dialog
          open={open}
          onClose={handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description">
          <DialogTitle id="alert-dialog-title">{"Delete Record"}</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              Are you sure,You want to delete this record.
                        </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose} color="primary">No</Button>
            <Button onClick={(e) => deleteCustomer(e)} color="primary" autoFocus>Yes</Button>
          </DialogActions>
        </Dialog>
      </div>
    </Container>
  );
}

export default memo(Home);