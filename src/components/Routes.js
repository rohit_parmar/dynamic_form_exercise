import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import Home from './Home';
import AddCustomer from './AddCustomer';
import CusotmerForm from './CusotmerForm';
import CustomerList from './CustomerList';

export default function Routes() {
    return (
        <Switch>
            <Route exact path='/' component={Home} />
            <Route exact path='/add-customer' component={AddCustomer} />

            <Route
                exact path='/edit-customer/:customerId'
                render={routeProps => (
                    <AddCustomer
                        customerId={routeProps.match.params.customerId}
                    />
                )}
            />
            <Route
                exact path='/customer/:customerId'
                render={routeProps => (
                    <CustomerList
                        customerId={routeProps.match.params.customerId}
                    />
                )}
            />
            <Route
                exact path='/customer/:customerId/add-record'
                render={routeProps => (
                    <CusotmerForm
                        customerId={routeProps.match.params.customerId}
                    />
                )}
            />
            <Route
                exact path='/customer/:customerId/edit-record/:recordId'
                render={routeProps => (
                    <CusotmerForm
                        customerId={routeProps.match.params.customerId}
                        recordId={routeProps.match.params.recordId}
                    />
                )}
            />
            <Redirect to='/' />

        </Switch>
    );
};
