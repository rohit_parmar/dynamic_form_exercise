import React, { useContext, memo } from 'react';
import clsx from 'clsx';
import Routes from './Routes';
import { CustomersContext } from '../context/AppContext';
import { useHistory, useLocation } from "react-router-dom";

import { makeStyles, useTheme } from '@material-ui/core/styles';

import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';



const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
    appBar: {
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: drawerWidth,
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    hide: {
        display: 'none',
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
    },
    drawerPaper: {
        width: drawerWidth,
    },
    drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(0, 1),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
        justifyContent: 'flex-end',
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(5),
        paddingTop: 80,
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        marginLeft: -drawerWidth,
    },
    contentShift: {
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
        marginLeft: 0,
    },
}));

function LeftSidebar() {
    const classes = useStyles();
    const theme = useTheme();
    const [open, setOpen] = React.useState(false);
    const customers = useContext(CustomersContext);

    let currentScreen = 'Home';
    let history = useHistory();
    let location = useLocation();

    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };

    function handleClick(id) {
        history.push("/customer/" + id);
    }

    function getCustomerName(customerId) {
        // eslint-disable-next-line eqeqeq
        let currentCustomer = customers.find((element) => { return element.id == customerId });
        if (currentCustomer && currentCustomer.customerName) {
            return currentCustomer.customerName;
        }
        return "";
    }

    function getCurrentScreenName() {
        currentScreen = 'Home';
        let currentPath = location.pathname;
        if (currentPath.includes('add-customer')) {
            currentScreen = 'Add Customer';
        } else if (currentPath.includes('customer') && currentPath.includes('add-record')) {
            let customerId = currentPath.split('/')[2];
            if (customerId) {
                currentScreen = 'Add Record ( ' + getCustomerName(customerId) + ' )';
            }
        } else if (currentPath.includes('customer') && currentPath.includes('edit-record')) {
            let customerId = currentPath.split('/')[2];
            if (customerId) {
                currentScreen = 'Edit Record ( ' + getCustomerName(customerId) + ' )';
            }
        } else if (currentPath.includes('edit-customer')) {
            let customerId = currentPath.substring(currentPath.lastIndexOf('/') + 1);
            if (customerId) {
                currentScreen = 'Edit Customer Form ( ' + getCustomerName(customerId) + ' )';
            }
        } else if (currentPath.includes('customer')) {
            let customerId = currentPath.substring(currentPath.lastIndexOf('/') + 1);
            if (customerId) {
                currentScreen = 'Customer Form ( ' + getCustomerName(customerId) + ' )';
            }
        }
        return currentScreen;
    }

    return (
        <div className={classes.root}>
            <CssBaseline />
            <AppBar
                position="fixed"
                className={clsx(classes.appBar, {
                    [classes.appBarShift]: open,
                })}>
                <Toolbar>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        onClick={handleDrawerOpen}
                        edge="start"
                        className={clsx(classes.menuButton, open && classes.hide)}>
                        <MenuIcon />
                    </IconButton>
                    <Typography variant="h6" noWrap>
                        {getCurrentScreenName()}
                    </Typography>
                </Toolbar>
            </AppBar>
            <Drawer
                className={classes.drawer}
                variant="persistent"
                anchor="left"
                open={open}
                classes={{
                    paper: classes.drawerPaper,
                }}
            >
                <div className={classes.drawerHeader}>
                    <IconButton onClick={handleDrawerClose}>
                        {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
                    </IconButton>
                </div>

                <Divider />
                <List>

                    <ListItem button key='home' onClick={(e) => handleClick('', e)}>
                        <ListItemText primary='Home' />
                    </ListItem>

                    {customers.map((todo, i) => (
                        <div key={i}>
                            <ListItem button key={i} onClick={(e) => handleClick(todo.id, e)}>
                                <ListItemText primary={todo.customerName} />
                            </ListItem>
                        </div>
                    ))}

                </List>
                <Divider />
            </Drawer>
            <main
                className={clsx(classes.content, {
                    [classes.contentShift]: open,
                })}
            >
                <Routes />
            </main>
        </div>
    );
}

export default memo(LeftSidebar);