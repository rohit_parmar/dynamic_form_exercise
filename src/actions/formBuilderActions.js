import { v4 as uuid } from 'uuid';
import addPropsToItem from './addPropsToItem';
import { 
  ADD_ITEM_TO_PREVIEW, 
  REMOVE_ITEM_FROM_PREVIEW, 
  SHOW_EDITOR,
  HIDE_EDITOR,
  SUBMIT_EDITOR_STATE,
  RESET_STATE
} from "./types";

export const addItem = element => {
  const id = uuid();
  const props = addPropsToItem(element);
  const item = { id, element, ...props };
  return { 
    type: ADD_ITEM_TO_PREVIEW, 
    payload: item 
  };
};

export const removeItem = id => (
  { 
    type: REMOVE_ITEM_FROM_PREVIEW, 
    payload: { 
      id 
    } 
  }
);

export const showEditor = item => (
  { 
    type: SHOW_EDITOR, 
    payload: { 
      item 
    } 
  }
);

export const hideEditor = () => (
  { 
    type: HIDE_EDITOR 
  }
);

export const resetState = () => (
  { 
    type: RESET_STATE 
  }
);

export const submitEditorState = (state) => (
  { 
    type: SUBMIT_EDITOR_STATE, 
    payload: { 
      state 
    } 
  }
);