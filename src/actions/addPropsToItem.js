import { v4 as uuid } from 'uuid';
const html = "Default Label";

export default (item) => {
  switch (item) {
    case 'Header':
    case 'Label':
      return {
        label: html
      };

    case 'Checkboxes':
      return {
        label: html,
        required: false,
        options: [
          {
            id: uuid(),
            value: 'Option1',
            checked: false
          },
          {
            id: uuid(),
            value: 'Option2',
            checked: false
          }
        ]
      };

    case 'Dropdown':
      return {
        label: html,
        required: false,
        value:'',
        options: [
          {
            id: uuid(),
            value: 'Option1'
          },
          {
            id: uuid(),
            value: 'Option2'
          }
        ],
      };

    case 'RadioButtons':
      return {
        required: false,
        label: html,
        options: [
          {
            id: uuid(),
            label: 'Label1',
            value: 'Value1',
            checked: false
          },
          {
            id: uuid(),
            label: 'Label2',
            value: 'Value2',
            checked: false
          }
        ]
      };

    case 'TextInput':
      return {
        required: false,
        label: html,
        value: ''
      };
    case 'Date':
      return {
        required: false,
        label: html,
        value: new Date(),
        maxDate: null,
        minDate: null
      }
    default:
      return;
  }
}