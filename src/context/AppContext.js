import React, { createContext, useReducer } from "react";
import { useLocalStorageReducer } from "../hooks/useLocalStorageReducer";
import todoReducer from "../reducers/customer.reducer.js";
import formReducer from "../reducers/formBuilder.reducer.js";
import { SnackbarProvider } from 'notistack';

export const CustomersContext = createContext();
export const DispatchContext = createContext();
export const FormContext = createContext();
export const FormDispatchContext = createContext();

const defaultCustomers = [
];

const initialState = {
  editorVisible: false,
  editorState: {},
  previewItems: []
};

export function AppProvider(props) {
  const [customers, dispatch] = useLocalStorageReducer(
    "customers",
    defaultCustomers,
    todoReducer
  );
  
  const [formState, formDispatch] = useReducer(formReducer, initialState);

  return (
    <SnackbarProvider
      hideIconVariant
      maxSnack={3}
      autoHideDuration={3000}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'center',
      }}>
      <CustomersContext.Provider value={customers}>
        <DispatchContext.Provider value={dispatch}>
          <FormContext.Provider value={formState}>
            <FormDispatchContext.Provider value={formDispatch}>
              {props.children}
            </FormDispatchContext.Provider>
          </FormContext.Provider>
        </DispatchContext.Provider>
      </CustomersContext.Provider >
    </SnackbarProvider>
  );
}
