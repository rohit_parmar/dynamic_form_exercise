import React from "react";
import ValidatedFormInputs from "./ValidatedFormInputs";

function Generator({ matrix, formData, onSubmit, editData }) {
    return (
        <ValidatedFormInputs
            matrix={matrix}
            formData={formData}
            onSubmit={onSubmit}
            editData={editData}
        />
    );
};
export default Generator;
