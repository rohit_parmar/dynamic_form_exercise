import React from "react";
import map from "lodash/map";
import { Button, Box, Container, Grid, Paper } from '@material-ui/core';
import { v4 as uuid } from 'uuid';
import { useSnackbar } from 'notistack';
import { InputTextField, DropdownField, CheckBoxField, RadioField, DateTextField } from './FormFields';

function ValidatedFormInputs({ matrix, formData, onSubmit, editData }) {
    const { enqueueSnackbar } = useSnackbar();

    function handleChange(event, id) {
        event.preventDefault();
        const { name, value } = event.target;
        if (!value && formData.some(elment => (elment.id === id && elment.required === true))) {
            enqueueSnackbar(name + ' : Field is Required', { variant: 'error' });
        }
    }

    function handleSubmit(e) {
        const formData = new FormData(e.target)
        const userFormData = {}
        e.preventDefault()

        if (!e.target.checkValidity()) {
            enqueueSnackbar('Form is invalid.', { variant: 'error' });
            return;
        }

        for (let entry of formData.entries()) {
            if (userFormData.hasOwnProperty(entry[0])) {
                userFormData[entry[0]] = [userFormData[entry[0]], entry[1]]
            } else {
                userFormData[entry[0]] = entry[1]
            }
        }
        var recordId = uuid();
        if (editData) {
            recordId = editData['recordId'];
        }
        userFormData['recordId'] = recordId;
        onSubmit(userFormData);
    }
    function getUpdatedOptions(options, value) {
        if (value) {
            options.forEach(item => {
                if (Array.isArray(value)) {
                    item.checked = value.some(obj => obj === item.value);
                } else {
                    item.checked = value.includes(item.value);
                }
            });
        }
        return options;
    }
    return (
        <Container maxWidth={(matrix === '12') ? "sm" : "md"}>
            <Paper style={{ margin: "1rem 0", padding: "1rem" }}>
                <form noValidate autoComplete="off" onSubmit={(values) => handleSubmit(values)}>
                    <Grid container spacing={3}>
                        {map(formData, formInput => {
                            const { id, label, element, required } = formInput;
                            let { options } = formInput;
                            let value = (editData ? editData[formInput.label] : '');
                            return (
                                <Grid item xs={matrix} key={id}>
                                    <Paper >
                                        <div key={formInput.id} style={{ padding: 10 }} >

                                            {/* -------------- INPUT TAG -------------- */}
                                            {
                                                element === "TextInput" && (
                                                    <InputTextField
                                                        id={id}
                                                        name={label}
                                                        value={value}
                                                        required={required}
                                                        onChange={(e, id) => handleChange(e, id)}
                                                    />
                                                )
                                            }

                                            {/* -------------- DROPDOWN -------------- */}
                                            {
                                                element === "Dropdown" && (
                                                    <DropdownField
                                                        id={id}
                                                        name={label}
                                                        required={required}
                                                        value={value}
                                                        options={options}
                                                    />
                                                )
                                            }

                                            {/* -------------- CHECKBOXES -------------- */}
                                            {
                                                element === "Checkboxes" && (
                                                    <CheckBoxField
                                                        id={id}
                                                        name={label}
                                                        required={required}
                                                        options={getUpdatedOptions(options, value)}
                                                    />
                                                )
                                            }

                                            {/* -------------- RADIO BUTTONS -------------- */}
                                            {
                                                element === "RadioButtons" && (
                                                    <RadioField
                                                        id={id}
                                                        name={label}
                                                        required={required}
                                                        options={options}
                                                        value={value}
                                                    />
                                                )
                                            }

                                            {/* -------------- DATE PICKER -------------- */}
                                            {
                                                element === "Date" && (
                                                    <DateTextField
                                                        id={id}
                                                        name={label}
                                                        required={required}
                                                        value={value}
                                                    />
                                                )
                                            }
                                        </div>
                                    </Paper>
                                </Grid>
                            );
                        })}

                    </Grid>
                    <Box display="flex" justifyContent="center" alignItems="flex-end" mt={2} mb={1}>
                        <Button variant="contained" type="submit" color="primary"> Submit </Button>
                    </Box>
                </form >
            </Paper>
        </Container>
    );
}

export default ValidatedFormInputs;