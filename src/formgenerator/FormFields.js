import React from 'react';
import {
    TextField, FormLabel, FormGroup,
    FormControlLabel, Checkbox, InputLabel,
    MenuItem, RadioGroup, Select, FormControl, Radio
} from '@material-ui/core';
import map from "lodash/map";

const InputTextField = ({ id, name, required, value, onChange }) => (
    <FormControl key={id} variant="filled" fullWidth required={required}>
        <TextField
            name={name}
            label={name}
            required={required}
            defaultValue={value}
            onChange={(e) => onChange(e, id)}
        />
    </FormControl>
);
const DropdownField = ({ id, name, required, options, value }) => (
    <FormControl key={id} fullWidth required={required}>
        <InputLabel id="demo-simple-select-filled-label"
        >{name}</InputLabel>
        <Select
            labelId="demo-simple-select-filled-label"
            id="demo-simple-select-filled"
            name={name}
            defaultValue={value}
        >
            {map(options, ({ id, value }) => (
                <MenuItem key={id} value={value}>{value}</MenuItem>
            ))}

        </Select>
    </FormControl>
);
function CheckBoxField({ id, name, required, options }) {
    const [state, setState] = React.useState(options);
    
    const handleChange = (event) => {
        let newState = state;
        newState = newState.map(item => {
            if (item.id === event.target.id) {
                item.checked = event.target.checked
            }
            return item;
        });
        setState(newState);
    };
    return (
        <FormControl key={id} variant="filled" fullWidth required={required}>
            <FormLabel component="legend" >{name}</FormLabel>
            <FormGroup>
                {map(state, ({ id, value, checked }) => (
                    <FormControlLabel key={id}
                        control={<Checkbox id={id} value={value} checked={checked} onChange={handleChange} />}
                        label={value}
                        name={name}
                    />
                ))}
            </FormGroup>
        </FormControl>
    );
}
const RadioField = ({ id, name, required, options, value }) => (
    <FormControl key={id} variant="filled" fullWidth required={required}>
        <FormLabel component="legend">{name}</FormLabel>
        <RadioGroup aria-label={name} name={name} defaultValue={value}>
            {map(options, ({ id, value }) => (
                <FormControlLabel key={id} value={value} control={<Radio />} label={value} />
            ))}

        </RadioGroup>
    </FormControl>
);

const DateTextField = ({ id, name, required, value }) => (
    <FormControl key={id} variant="filled" fullWidth noValidate required={required}>
        <TextField
            id='date'
            label={name}
            name={name}
            type="date"
            required={required}
            defaultValue={value}
            InputLabelProps={{
                shrink: true,
            }}
        />
    </FormControl>
);

export { InputTextField, DropdownField, CheckBoxField, RadioField, DateTextField };