const reducer = (state, action) => {
  switch (action.type) {
    case "ADD":
      return [...state,
      {
        id: action.id,
        customerName: action.customerName,
        customerForm: action.customerForm,
        customerRecords: action.customerRecords,
        customerMatrix: action.customerMatrix
      }];
    case "EDIT_CUSTOMER":
      return state.map(customer =>
        customer.id === action.id ? {
          ...customer, customerName: action.customerName,
          customerForm: action.customerForm,
          customerMatrix: action.customerMatrix,
          customerRecords: []
        } : customer
      );
    case "EDIT":
      return state.map(customer =>
        customer.id === action.id ? { ...customer, customerRecords: action.customerRecords } : customer
      );
    case "DELETE":
      return state.filter(customer => customer.id !== action.id);
    default:
      return state;
  }
};
export default reducer;
