import React from "react";
import isEmpty from "lodash/isEmpty";
import FormInputs from "./FormInputs";
// import { FormContext } from '../../context/AppContext';
import Paper from "@material-ui/core/Paper";
import { Container } from '@material-ui/core';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Box from '@material-ui/core/Box';

function Preview({ hovered, previewItems }) {

  // const formState = useContext(FormContext);
  // let previewItems = formState.previewItems;

  return (

    <Container>
      <Paper style={{ margin: "1rem 0", padding: "1rem", minHeight: "50vh" }}>
        {isEmpty(previewItems) && (
          <Box borderRadius={8} boxShadow={3}>
            <List component="nav" border={1} aria-label="main mailbox folders">
              <ListItem button border={1}>
                <ListItemText primary='Select an item from Toolbox' />
              </ListItem>
            </List>
          </Box>
        )}

        {!isEmpty(previewItems) &&
          previewItems.map((item, i) => (
            <FormInputs
              index={i}
              item={item}
              id={item.id}
              key={item.id}
            />
          ))}
      </Paper>
    </Container>
  );
}
export default Preview;