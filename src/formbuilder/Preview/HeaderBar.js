import React, { useContext } from "react";
import EditIcon from '@material-ui/icons/EditSharp';
import DeleteIcon from '@material-ui/icons/DeleteForeverSharp';
import Box from '@material-ui/core/Box';
import {
  removeItem,
  showEditor
} from "../../actions/formBuilderActions";
import { FormDispatchContext } from '../../context/AppContext';
import IconButton from '@material-ui/core/IconButton';

function HeaderBar({ item, id }) {
  const formDispatch = useContext(FormDispatchContext);
  function removeItemEvent(id) {
    formDispatch(removeItem(id));
  }
  function showEditorEvent(item) {
    formDispatch(showEditor(item));
  }
  return (
    <div style={{ width: '100%' }}>
      <Box display="flex" p={1}>
        <Box p={1} flexGrow={1} >
          <h3>{item.element}</h3>
        </Box>
        <Box p={1} >
          <IconButton aria-label="delete" onClick={() => showEditorEvent(item)} >
            <EditIcon color="primary" />
          </IconButton>
        </Box>
        <Box p={1} >
          <IconButton aria-label="delete" onClick={() => removeItemEvent(id)}>
            <DeleteIcon color="primary" />
          </IconButton>
        </Box>
      </Box>
    </div>
  );
}

export default HeaderBar;
