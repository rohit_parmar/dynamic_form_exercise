import React from "react";
import { InputTextField, DropdownField, CheckBoxField, RadioField, DateTextField } from './FormFields';

export default (item) => {
  switch (item.element) {
    case "Checkboxes":
      return <CheckBoxField item={item} />;
    case "Dropdown":
      return <DropdownField item={item} />;
    case "RadioButtons":
      return <RadioField item={item} />;
    case "TextInput":
      return <InputTextField item={item} />;
    case "Date":
      return <DateTextField item={item} />;
    default:
      return "";
  }
};
