import React from 'react';
import {
    TextField, FormLabel, FormGroup,
    FormControlLabel, Checkbox, InputLabel,
    MenuItem, RadioGroup, Select, FormControl, Radio
} from '@material-ui/core';
import map from "lodash/map";

const InputTextField = ({ item }) => (
    <FormControl key={item.id} variant="filled" fullWidth required={item.required}>
        <TextField
            name={item.className}
            label={item.label}
            required={item.required}
            defaultValue={item.value}
        />
    </FormControl>
);
const DropdownField = ({ item }) => (
    <FormControl key={item.id} fullWidth required={item.required}>
        <InputLabel id="demo-simple-select-filled-label"
        >{item.label}</InputLabel>
        <Select
            labelId="demo-simple-select-filled-label"
            id="demo-simple-select-filled"
            name={item.name}
            defaultValue={item.value}>
            {map(item.options, ({ id, value }) => (
                <MenuItem key={id} value={value}>{value}</MenuItem>
            ))}

        </Select>
    </FormControl>
);
const CheckBoxField = ({ item }) => (
    <FormControl key={item.id} variant="filled" fullWidth required={item.required}>
        <FormLabel component="legend" >{item.label}</FormLabel>
        <FormGroup>
            {map(item.options, ({ id, value, checked }) => (
                <FormControlLabel key={id}
                    control={<Checkbox name={value} />}
                    label={value}
                    name={value}
                />
            ))}
        </FormGroup>
    </FormControl>
);
const RadioField = ({ item }) => (
    <FormControl key={item.id} variant="filled" fullWidth required={item.required}>
        <FormLabel component="legend">{item.label}</FormLabel>
        <RadioGroup aria-label={item.name} name={item.name} defaultValue={item.value}>
            {map(item.options, ({ id, label,value }) => (
                <FormControlLabel key={id} value={value} control={<Radio />} label={label} />
            ))}

        </RadioGroup>
    </FormControl>
);

const DateTextField = ({ item }) => (
    <FormControl key={item.id} variant="filled" fullWidth noValidate required={item.required}>
        <FormLabel component="legend">{item.label}</FormLabel>
        <TextField
            id='date'
            label={item.name}
            name={item.name}
            type="date"
            required={item.required}
            defaultValue={item.value}
            InputLabelProps={{
                shrink: true,
            }}
        />
    </FormControl>
);

export { InputTextField, DropdownField, CheckBoxField, RadioField, DateTextField };