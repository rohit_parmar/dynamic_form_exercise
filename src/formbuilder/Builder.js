import React from 'react';
import FormEditor from './FormEditor/FormEditor';
import Toolbar from './Toolbar/Toolbar';
import Preview from './Preview/Preview';
// import { FormContext } from '../context/AppContext';
import Box from '@material-ui/core/Box';

function Builder({ previewItems, editorVisible }) {
  // const formState = useContext(FormContext);
  // const editorVisible = formState.editorVisible;
  return (
    <React.Fragment>
      {
        editorVisible &&
        <FormEditor open={editorVisible} />
      }
      <div style={{ width: '100%' }}>
        <Box display="flex" p={1} >
          <Box p={1} width="20%" pt={5} >
            <Toolbar />
          </Box>
          <Box p={1} width="70%" >
            <Preview previewItems={previewItems} />
          </Box>
        </Box>
      </div>
    </React.Fragment>
  );
}
export default Builder;