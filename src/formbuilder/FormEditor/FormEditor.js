import React, { useContext, useState } from "react";
import { v4 as uuid } from 'uuid';
import { map, filter } from "lodash";
import {
  hideEditor,
  submitEditorState
} from "../../actions/formBuilderActions";
import { FormContext, FormDispatchContext } from '../../context/AppContext';

import {
  Box, Dialog, FormControlLabel, FormGroup, Checkbox, FormControl,
  TextField, DialogActions, FormLabel, DialogContent, Button, DialogTitle
} from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/DeleteForeverSharp';


function FormEditor({ open }) {

  const formState = useContext(FormContext);
  const formDispatch = useContext(FormDispatchContext);

  const [state, setState] = useState(formState);
  const {
    editorState,
    editorState: {
      element,
      options,
      required,
      label,
    }
  } = state;

  function toggleField(field) {
    setState(prevState => ({
      editorState: {
        ...state.editorState,
        [field]: !prevState.editorState[field]
      }
    }));
  };
  function handleLableChange(value) {
    setState({
      editorState: {
        ...state.editorState,
        label: value
      }
    });
  };

  function handleChange(value, optionId, field) {
    const { options } = editorState;
    const updatedOptions = map(options, option => {
      if (option.id === optionId) {
        option[field] = value;
        return option;
      }
      return option;
    });
    setState({
      editorState: {
        ...state.editorState,
        options: updatedOptions
      }
    });
  };


  function addOption(element) {
    let option;
    switch (element) {
      case 'Checkboxes': {
        option = {
          id: uuid(),
          value: "",
          checked: false
        }
        break;
      }
      case 'RadioButtons': {
        option = {
          id: uuid(),
          label: "",
          value: "",
          checked: false
        }
        break;
      }
      case 'Dropdown': {
        option = {
          id: uuid(),
          value: ""
        }
        break;
      }
      default: {
        option = {
          id: uuid(),
          value: ""
        }
        break;
      }
    }

    const { options } = formState.editorState;
    const updatedOptions = [...options, option];

    setState({
      editorState: {
        ...state.editorState,
        options: updatedOptions
      }
    });

  };

  function removeOption(optionId) {
    const { options } = state.editorState;
    let updatedOptions = [...options];
    if (options.length > 1) {
      updatedOptions = filter(
        options,
        option => option.id !== optionId
      );
    }

    setState({
      editorState: {
        ...state.editorState,
        options: updatedOptions
      }
    });
  };

  function handleSubmit(label) {
    formDispatch(submitEditorState({ ...state.editorState }));
  };

  function hideEditorEvent() {

    formDispatch(hideEditor());
  }


  return (
    <Dialog
      disableBackdropClick
      disableEscapeKeyDown
      fullWidth
      maxWidth='sm'
      onClose={() => hideEditorEvent()}
      open={open}
      aria-labelledby="form-dialog-title">
      <Box>
        <DialogTitle id="form-dialog-title">{element} Editor</DialogTitle>


        {/* ------------- LABEL ------------- */}
        <DialogContent>
          <FormLabel component="legend" >Change Label Text :</FormLabel>
          <TextField
            defaultValue={label}
            fullWidth
            style={{ paddingTop: 10 }}
            onChange={e =>
              handleLableChange(e.target.value)
            }
          />
        </DialogContent>
        {/* ------------- LABEL ------------- */}

        {/* ------------- REQUIRED ------------- */}
        {editorState.hasOwnProperty("required") && (
          <DialogContent>
            <FormControl variant="filled" fullWidth style={{ paddingTop: 10 }}  >
              <FormLabel component="legend" >Set field Required :</FormLabel>
              <FormGroup style={{ paddingTop: 10 }}>
                <FormControlLabel key='required'
                  control={<Checkbox name='Required' checked={required} />}
                  label='Required'
                  name='Required'
                  onChange={() => toggleField("required")}
                />
              </FormGroup>
            </FormControl>
          </DialogContent>
        )}
        {/* ------------- REQUIRED ------------- */}

        {/* ------------- DROPDOWN & RADIO OPTIONS ------------- */}
        {(element === "Dropdown" || element === "Checkboxes") && (
          <DialogContent>
            <FormControl variant="filled" fullWidth style={{ paddingTop: 10 }}  >
              <FormLabel component="legend" >Options:</FormLabel>
              <FormGroup style={{ paddingTop: 10 }}>
                {map(options, ({ id, value }) => (

                  <div style={{ width: '100%' }} key={id}>
                    <Box display="flex" p={1}>
                      <Box p={1} flexGrow={1} >
                        <TextField
                          defaultValue={value}
                          placeholder="Option"
                          fullWidth
                          onChange={e => handleChange(e.target.value, id, "value")}
                        />
                      </Box>
                      <Box p={1} >
                        <IconButton aria-label="delete" disabled={options.length === 1} onClick={() => { removeOption(id); }}>
                          <DeleteIcon />
                        </IconButton>
                      </Box>
                    </Box>
                  </div>
                ))}
              </FormGroup>
              <Button onClick={() => addOption(element)} variant="outlined" color="primary"> Add Option</Button>
            </FormControl>
          </DialogContent>
        )}
        {/* ------------- DROPDOWN & RADIO OPTIONS ------------- */}

        {/* ------------- RADIO BUTTON OPTIONS ------------- */}
        {element === "RadioButtons" && (
          <DialogContent>
            <FormControl variant="filled" fullWidth style={{ paddingTop: 10 }}  >
              <FormLabel component="legend" >Options:</FormLabel>
              <FormGroup style={{ paddingTop: 10 }}>
                {map(options, ({ id, value, label }) => (

                  <div style={{ width: '100%' }} key={id}>
                    <Box display="flex" p={1}>
                      <Box p={1} flexGrow={1} >
                        <TextField
                          defaultValue={label}
                          placeholder="Label"
                          fullWidth
                          onChange={e => handleChange(e.target.value, id, "label")}
                        />
                      </Box>
                      <Box p={1} flexGrow={1} >
                        <TextField
                          defaultValue={value}
                          placeholder="Value"
                          fullWidth
                          onChange={e => handleChange(e.target.value, id, "value")}
                        />
                      </Box>
                      <Box p={1} >
                        <IconButton aria-label="delete" disabled={options.length === 1} onClick={() => { removeOption(id); }}>
                          <DeleteIcon />
                        </IconButton>
                      </Box>
                    </Box>
                  </div>
                ))}
              </FormGroup>
              <Button onClick={() => addOption(element)} variant="outlined" color="primary"> Add Option</Button>
            </FormControl>
          </DialogContent>
        )}
        {/* ------------- RADIO BUTTON OPTIONS ------------- */}

        <DialogActions>
          <Button onClick={() => hideEditorEvent()} color="primary">
            Cancel
          </Button>
          <Button onClick={() => handleSubmit(label)} variant="contained" color="primary">
            Submit
          </Button>
        </DialogActions>
      </Box>
    </Dialog >
  );

}
export default FormEditor;