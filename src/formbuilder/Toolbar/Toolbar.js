import defaultItems from "./defaultItems";
import React, { useContext } from "react";
import { addItem } from "../../actions/formBuilderActions";
import { FormDispatchContext } from '../../context/AppContext';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Box from '@material-ui/core/Box';
import { Divider } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
}));


export default function Toolbar() {
  const classes = useStyles();
  const formDispatch = useContext(FormDispatchContext);
  function addItemToContext(data) {
    formDispatch(addItem(data.key));
  }

  const items = defaultItems();

  return (
    <div className={classes.root}>
      <Box borderRadius={8} boxShadow={3}>
        <List component="nav" border={1} aria-label="main mailbox folders">
          {items.map(item => (
            <div key={item.key}>
              <ListItem button border={1} key={item.key} onClick={() => addItemToContext(item)}>
                <ListItemIcon >
                  <item.icon color="primary" />
                </ListItemIcon>
                <ListItemText primary={item.name} />
              </ListItem>
              <Divider />
            </div>
          ))}
        </List>
      </Box>
    </div>
  );
}


