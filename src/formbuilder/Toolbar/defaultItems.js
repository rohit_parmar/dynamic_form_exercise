import AccessTimeSharpIcon from '@material-ui/icons/AccessTimeSharp';
import TextFormatSharpIcon from '@material-ui/icons/TextFormatSharp';
import ArrowDropDownCircleSharpIcon from '@material-ui/icons/ArrowDropDownCircleSharp';
import CheckBoxSharpIcon from '@material-ui/icons/CheckBoxSharp';
import RadioButtonCheckedSharpIcon from '@material-ui/icons/RadioButtonCheckedSharp';

export default () => [
  {
    key: "TextInput",
    name: "Text Input",
    icon: TextFormatSharpIcon
  },
  {
    key: "Dropdown",
    name: "Dropdown",
    icon: ArrowDropDownCircleSharpIcon
  },
  {
    key: "Checkboxes",
    name: "Checkboxes",
    icon: CheckBoxSharpIcon
  },
  {
    key: "RadioButtons",
    name: "RadioButtons",
    icon: RadioButtonCheckedSharpIcon
  },
  {
    key: "Date",
    name: "Date",
    icon: AccessTimeSharpIcon
  }
];
